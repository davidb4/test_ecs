# https://github.com/j8r/dockerfiles/blob/master/systemd/ubuntu/20.04.Dockerfile
FROM jrei/systemd-ubuntu:20.04

ENV LC_ALL C
ENV DEBIAN_FRONTEND noninteractive

RUN sed -i 's/# deb/deb/g' /etc/apt/sources.list

RUN apt-get update \
    && apt-get install -y wget iproute2 cron git libfreetype6-dev libpng-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN wget https://packages.chef.io/files/stable/chef-server/14.0.65/ubuntu/20.04/chef-server-core_14.0.65-1_amd64.deb \
    && dpkg -i chef-server-core_14.0.65-1_amd64.deb \
    && rm chef-server-core_14.0.65-1_amd64.deb

VOLUME [ "/sys/fs/cgroup" ]

CMD ["/lib/systemd/systemd"]
#     && chef-server-ctl reconfigure --chef-license=accept
    
# RUN chef-server-ctl org-create playtrak "PLAYTRAK Sistemas de Monitoreo" -f chef-server.pem \
#     && chef-server-ctl user-create grantmc Grant McLennan grantmc@chef.io p@s5w0rD! -f /tmp/grantmc.pem \
#     && chef-server-ctl org-user-add playtrak grantmc --admin
