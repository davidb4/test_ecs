Poner a correr el contenedor en modo _detach_ y conectarse para ejecutar comandos:

```sh
docker run -d --name chef-server --privileged --tmpfs /tmp --tmpfs /run --tmpfs /run/lock -v /sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/davidb4/test_ecs:master
docker exec -ti chef-server bash
```

Dentro del contenedor finalizar la instalación de chef:

```sh
chef-server-ctl reconfigure --chef-license=accept
chef-server-ctl org-create playtrak "PLAYTRAK Sistemas de Monitoreo" -f chef-server.pem 
chef-server-ctl user-create grantmc Grant McLennan grantmc@chef.io p@s5w0rD! -f /tmp/grantmc.pem 
chef-server-ctl org-user-add playtrak grantmc --admin
```